<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;

class UsuarioController extends Controller
{
    public function getUsuarios()
    {
        $usuarios = User::all();

        return $usuarios->tojson();
    }
}
